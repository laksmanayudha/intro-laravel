@extends('layout.master')

@section('content')
<div class="container mt-4">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Create New Questions</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="title" value="{{ old('judul', '') }}" name="judul" placeholder="Enter title">
                </div>
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="body">isi</label>
                    <input type="text" class="form-control" id="body" value="{{ old('isi', '') }}" name="isi" placeholder="Body">
                </div>
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
</div>


@endsection