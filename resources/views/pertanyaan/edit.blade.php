@extends('layout.master')

@section('content')
<div class="container mt-4">
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Edit Questions {{ $pertanyaan->id }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="title" value="{{ old('judul', $pertanyaan->judul) }}" name="judul" placeholder="Enter title">
                </div>
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="body">isi</label>
                    <input type="text" class="form-control" id="body" value="{{ old('body', $pertanyaan->isi) }}" name="isi" placeholder="Body">
                </div>
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</div>


@endsection