<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form sign up</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    <div>

        <form action="/welcome" method="POST">
            @csrf
            <label for="firstName"> First name :</label><br><br>
            <input type="text" id="firstName" name="firstName"> <br><br>

            <label for="lastName"> Last name :</label><br><br>
            <input type="text" id="lastName" name="lastName"> <br><br>

            <label > Gender:</label><br><br>
            <input type="radio" name="gender" checked> Laki - Laki <br>
            <input type="radio" name="gender"> Perempuan <br>
            <br>

            <label>Nationality:</label> <br>
            <select name="" id="">
                <option value="Indonesian">Indonesian</option>
                <option value="American">American</option>
                <option value="Australian">Australian</option>
            </select>
            <br><br>

            <label > Language Spoken:</label><br><br>
            <input type="checkbox" name="language" value="0" checked> Indonesia <br>
            <input type="checkbox" name="language" value="1"> English <br>
            <input type="checkbox" name="language" value="2"> Other <br>

            <label for="bio"> Bio :</label><br><br>
            <textarea name="" id="bio" cols="30" rows="10"></textarea> <br><br>

            <input type="submit" value="Sign Up">

        </form>

    </div>
</body>
</html>